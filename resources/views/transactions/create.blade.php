@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => __('User Profile')])
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />


@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <form method="post" action="{{ route('transaction.store') }}" autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('post')

                        <div class="card ">
{{--                            <div class="card-header card-header-primary">--}}
{{--                                <h4 class="card-title">{{ __('Create User Account') }}</h4>--}}
{{--                                <p class="card-category">{{ __('User information') }}</p>--}}
{{--                            </div>--}}
                            <div class="card-body ">
                                @if (session('status'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('status') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', $errors->has('name')) }}" required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->has('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Goods') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
{{--                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', $errors->has('name')) }}" required="true" aria-required="true"/>--}}
                                            <select id="country" style="width:300px;">
                                                <!-- Dropdown List Option -->
                                            </select>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->has('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Description') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                            <select class="form-control col-md-12" name="cars" id="cars">
                                                <option value="volvo">Volvo</option>
                                                <option value="saab">Saab</option>
                                                <option value="mercedes">Mercedes</option>
                                                <option value="audi">Audi</option>
                                            </select>
{{--                                            <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="input-description" placeholder="{{ __('Say something descriptive of the product...') }}" value="{{ old('phone', $errors->has('phone')) }}" required="true"></textarea>--}}
{{--                                            <input class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="input-description" type="text" placeholder="{{ __('081XXXX...') }}" value="{{ old('phone', $errors->has('phone')) }}" required="true"/>--}}
                                            @if ($errors->has('phone'))
                                                <span id="phone-error" class="error text-danger" for="input-phone">{{ $errors->has('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Items')  }}</label>
                                    <div class="col-sm-7">
{{--                                        <div class="form-group{{ $errors->has('Items') ? ' has-danger' : '' }}">--}}
{{--                                            <div class="design_content_form influencer_form">--}}
{{--                                                <select name="content[2][platform]" id="content3">--}}
{{--                                                    <option value="Twitter">Twitter</option>--}}
{{--                                                    <option value="Facebook">Facebook</option>--}}
{{--                                                    <option value="Instagram">Instagram</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                                <input type="text" name="content[2][handle]" placeholder="Enter Influencers account here">--}}
                                            <div class="add_input">
{{--                                                <p>Have more information to fill? </p>--}}
                                                <a href="javascript:void(0)" class="add_button influence__btn">Add New input</a>
                                            </div>
{{--                                        </div>--}}
                                    </div>
                                </div>
{{--                                <div class="row">--}}
{{--                                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>--}}
{{--                                    <div class="col-sm-7">--}}
{{--                                        <div class="form-group{{ $errors->first('email') ? ' has-danger' : '' }}">--}}
{{--                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" value="{{ old('email', $errors->first('email')) }}" required />--}}
{{--                                            @if ($errors->has('email'))--}}
{{--                                                <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <label class="col-sm-2 col-form-label" for="input-password">{{ __('New Password') }}</label>--}}
{{--                                    <div class="col-sm-7">--}}
{{--                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">--}}
{{--                                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ __('New Password') }}" value="" required />--}}
{{--                                            @if ($errors->has('password'))--}}
{{--                                                <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ __('Confirm New Password') }}</label>--}}
{{--                                    <div class="col-sm-7">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ __('Confirm New Password') }}" value="" required />--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var country = ["Australia", "Bangladesh", "Denmark", "Hong Kong", "Indonesia", "Netherlands", "New Zealand", "South Africa"];
            $("#country").select2({
                data: country
            });
        });
    </script>
@endsection
