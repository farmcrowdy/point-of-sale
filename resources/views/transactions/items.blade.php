@extends('layouts.app', ['activePage' => 'transactions', 'titlePage' => __('Transactions')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Transaction Items</h4>
                            <p class="card-category"> Here you can manage transaction items</p>
                        </div>
                        <div class="card-body">
{{--                            <div class="row">--}}
{{--                                <div class="col-12 text-right">--}}
{{--                                    <a href="#" class="btn btn-sm btn-primary">Add Transaction</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th class="text-right">Creation date</th>
{{--                                        <th class="text-right">Actions</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactionItems as $transactionItem)
                                        <tr>
                                            <td>{{ $transactionItem->product->name }}</td>
{{--                                            <td><a href="{{ url('transaction/view', $transactionItem->id) }}">{{ $transactionItem->reference }}</a></td>--}}
                                            <td>&#8358;{{ $transactionItem->product->price }}</td>
                                            <td class="text-right">{{ $transactionItem->created_at }}</td>
{{--                                            <td class="td-actions text-right">--}}
{{--                                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ url('transaction/view', $transactionItem->id) }}" data-original-title="" title="">--}}
{{--                                                    <i class="material-icons">visibility</i>--}}
{{--                                                    <div class="ripple-container"></div>--}}
{{--                                                </a>--}}
{{--                                            </td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            md.initDashboardPageCharts();
        });
    </script>
@endpush
