<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionsController;
use App\Models\UserType;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('home', 'Web\HomeController@index');
//Route::get('user_types', function () {
//    return 'e dey work?';
//});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth'], function() {
    Route::get('/user_types', [UserTypeController::class, 'user_types'])->name('user_types');
    Route::get('/configs', [ConfigController::class, 'index'])->name('configs');
    Route::get('/products', [ProductController::class, 'index'])->name('products');
    Route::get('/transactions', [TransactionsController::class, 'index'])->name('transactions');
    Route::get('/transaction/view/{transaction_id}', [TransactionsController::class, 'view']);
    Route::get('/transaction/create', [TransactionsController::class, 'create'])->name('transaction.create');
    Route::post('/transaction/store', [TransactionsController::class, 'store'])->name('transaction.store');

});
Auth::routes();

//Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});


Route::group(['middleware' => 'auth'], function () {
//	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
//	Route::resource('user', [App\Http\Controllers\UsersController::class, 'index']);
//	Route::get('user', [App\Http\Controllers\UsersController::class, 'index'])->name('user');

    Route::get('user', [UserController::class, 'index'])->name('user.index');
    Route::get('user/edit/{user_id}', [UserController::class, 'edit_user']);
    Route::get('user/create', [UserController::class, 'create_user']);

    Route::get('profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::post('profile/create', [ProfileController::class, 'create'])->name('profile.create');

    Route::put('profile/password', [App\Http\Controllers\ProfileController::class, 'password'])->name('profile.password');


//	Route::get('profile', ['uses' => 'App\Http\Controllers\ProfileController@edit']);
//	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
//	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

