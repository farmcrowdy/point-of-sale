//add more input for Content of the Design in flyers page
$(document).ready(function () {
    var maxField = 4; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button link
    var wrapper = $('.add_input'); //Input field wrapper
    var fieldHTML = `<div class="new_input_flex"><input type="text" placeholder="Fill in the information you want here"><a href="javascript:void(0);" class="remove_button">-</a></div>`
    let influencerFieldHTML =(x)=>{
        x = x + 1
        return `<div class="new_input_flex influencers_btn">
                                    <input type="text" class="sm_platform" name="content[${x}][platform]" placeholder="Platform">
                                    <div>
                                        <input type="text" name="content[${x}][handle]" placeholder="Enter Influencers account here"><a href="javascript:void(0);" class="remove_button">-</a>
                                    </div>
                                </div>`
    }
    var x = 1; //Initial field counter is 1
    //Once add button is clicked
    $(addButton).click(function (e) {
        // console.log(e.target.classList.include("influence__btn"))
        wrapper = $(this).parent('.add_input');
        let classStyle = e.target.classList.contains("influence__btn")
        if(classStyle){
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                // $(wrapper).append(influencerFieldHTML); //Add field html
                $(wrapper).append(influencerFieldHTML(x)); //Add field html
            }
        }else{
            //Check maximum number of input fields
            if (x < maxField) {
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        }

    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parents('div.new_input_flex').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
