<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Date;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactions = [
            [
                'user_id' => 1,
                'reference' => '#' . rand(0, 99999999),
            ],
            [
                'user_id' => 1,
                'reference' => '#' . rand(0, 99999999),
            ]
        ];

        foreach ($transactions as $transaction) {
            DB::table('transactions')->insert([
                'user_id' => $transaction['user_id'],
                'reference' => $transaction['reference'],
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ]);
        }
    }
}
