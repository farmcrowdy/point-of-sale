<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            [
                'name' => 'site_name',
                'value' => 'Farmcrowdy P.O.S. Manager',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'site_description',
                'value' => 'Farmcrowdy P.O.S. Manager',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ];

        foreach ($configs as $config) {
            DB::table('user_types')->insert([
                'name' => $config['name'],
                'value' => $config['value'],
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
