<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name' => 'Millet',
                'description' => 'This is a great product',
                'price' => 100,
                'image' => 'logo.png'
            ],
            [
                'name' => 'Groundnut',
                'description' => 'This is a great product',
                'price' => 100,
                'image' => 'logo.png'
            ]
        ];

        foreach ($products as $product) {
            DB::table('products')->insert([
               'name' => $product['name'],
               'description' => $product['description'],
               'price' => $product['price'],
               'image' => $product['image'],
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ]);
        }
    }
}
