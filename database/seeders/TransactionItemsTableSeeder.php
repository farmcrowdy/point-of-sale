<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Date;

class TransactionItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactionItems = [
            [
                'transaction_id' => 1,
                'product_id' => 1,
                'units' => 1
            ],
            [
                'transaction_id' => 1,
                'product_id' => 2,
                'units' => 1
            ],
            [
                'transaction_id' => 2,
                'product_id' => 1,
                'units' => 2
            ],
        ];

        foreach ($transactionItems as $transactionItem) {
            DB::table('transaction_items')->insert([
                'transaction_id' => $transactionItem['transaction_id'],
                'product_id' => $transactionItem['product_id'],
                'units' => $transactionItem['units'],
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ]);
        }
    }
}
