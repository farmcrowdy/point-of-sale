<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $users = User::all();
        $activePage = 'Users';
        return view('users.index', ['users' => $users, 'activePage' => $activePage]);
    }

    public function create_user()
    {
        $activePage = 'Create User';
        return view('users.create', ['activePage' => $activePage]);
    }

    public function edit_user(int $user_id) {
        $user = User::where('id', $user_id)->get();
        if(sizeof($user) > 0) $user = $user[0];
        $activePage = '';
        return view('users.show', ['user' => $user, 'activePage' => $activePage]);
    }
}
