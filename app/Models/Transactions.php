<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Transactions extends Model
{
    use HasFactory;

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function items()
    {
//        return TransactionItems::where('id', $transaction_id)->get();
        return $this->hasMany(TransactionItems::class, 'transaction_id', 'id');
    }


}
