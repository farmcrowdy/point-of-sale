<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionItems extends Model
{
    use HasFactory;

//    public function items()
//    {
//        return $this->belongsTo(Transactions::class, 'transaction_id', 'id');
//    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
